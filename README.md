<<<<<<< HEAD
# redis-redpack

#### 介绍
基于redis加lua脚本实现高并发下的抢红包设计

测试入口：genRedpack()生成红包，snatchRedpack()抢红包

将127.0.0.1替换成你的redis地址。

```java
public class TestRedpackService {

    @Test
    public void genRedpack(){
        JedisUtils jedisUtils = new JedisUtils("127.0.0.1", 6379, "123456");
        RedpackService redpackService = new RedpackService(jedisUtils);
        redpackService.genRedpack(111111,5);
    }

    @Test
    public void snatchRedpack() throws InterruptedException {
        JedisUtils jedisUtils = new JedisUtils("127.0.0.1", 6379, "123456");
        RedpackService redpackService = new RedpackService(jedisUtils);
        IdWorker idWorker = new IdWorker();
        int N = 100;
        CyclicBarrier barrier = new CyclicBarrier(N);

        for (int i = 0;i<N;i++){
            new Thread(()->{
                try {
                    System.out.println(Thread.currentThread().getName()+"准备抢红包");
                    barrier.await();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (BrokenBarrierException e) {
                    e.printStackTrace();
                }
                String result = redpackService.snatchRedpack(idWorker.nextId(), 111111);
                if ("0".equals(result)){
                    System.out.println(Thread.currentThread().getName() + "未抢到红包，原因：红包已领完");
                }else if ("1".equals(result)){
                    System.out.println(Thread.currentThread().getName() + "未抢到红包，原因：红包已领过");
                }else{
                    System.out.println(Thread.currentThread().getName() + "抢到红包：" + result);
                }
            },"thread"+i).start();
        }
        Thread.sleep(Integer.MAX_VALUE);
    }

}
```



详细设计说明，可访问我的博客：https://my.oschina.net/suzheworld/blog/2995288
=======
# redis

#### 介绍
redis整体文件存放

#### 软件架构
软件架构说明


#### 安装教程

1.  xxxx
2.  xxxx
3.  xxxx

#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 码云特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5.  码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
>>>>>>> 090b7fa7f60263f49b5174c401f509e0e8b3d0ea
